package es.uniovi.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    double mEuroToDollar;
    EditText mEditTextEuros,mEditTextDollars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mEuroToDollar = 1.16;


        mEditTextEuros = (EditText) findViewById(R.id.editTextEuros);
        mEditTextDollars = (EditText) findViewById(R.id.editTextDollars);

        // Crear la cola de peticiones HTTP
        RequestQueue queue = Volley.newRequestQueue(this);

// Crear una petición
        String url ="http://data.fixer.io/api/latest?access_key=9045c3084494265c4c97ea411532a851&symbols=USD";
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,null,
                // Implementar el interfaz Listener que debe tener el método
                // onResponse, que será llamado al recibir la respuesta del servidor
                // Este método se ejecutará en el hilo del GUI
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            mEuroToDollar=response.getJSONObject("rates").getDouble("USD");
                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                // Implementar el interfaz ErrorListener, que debe tener el método
                // onErrorResponse, que será llamado si la respuesta del servidor no es 200 OK
                // También se ejecutará en la interfaz de usuario
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Volley: ha ocurrido un error " + error);
                    }
                }
        );
// Poner la petición en la cola
        queue.add(stringRequest);





    }

    // Se llama cuando el usuario pulsa el botón
    public void onClickToDollars(View view) {
        // Hacer algo en respuesta a la pulsación
        //Toast.makeText(getApplicationContext(), "Me han pulsado $",Toast.LENGTH_SHORT).show();
        convert(mEditTextEuros,mEditTextDollars,mEuroToDollar);
    }

    // Se llama cuando el usuario pulsa el botón
    public void onClickToEuros(View view) {
        // Hacer algo en respuesta a la pulsación
        //Toast.makeText(getApplicationContext(), "Me han pulsado €",Toast.LENGTH_SHORT).show();
        convert(mEditTextDollars,mEditTextEuros,1/mEuroToDollar);
    }

    void convert(EditText editTextSource, EditText editTextDestination,
                 double ConversionFactor) {

        String StringSource = editTextSource.getText().toString();

        double NumberSource;
        try {
            NumberSource = Double.parseDouble(StringSource);
        } catch (NumberFormatException nfe) {
            return;
        }
        double NumberDestination = NumberSource * ConversionFactor;

        String StringDestination = Double.toString(NumberDestination);

        editTextDestination.setText(StringDestination);
    }
}
